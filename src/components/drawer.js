import React from 'react'
import { Drawer, Button, Icon, Menu } from 'antd';
import menuIcon from './menu-icon.svg';

class MenuDrawer extends React.Component {
  state = { visible: false, placement: 'left' };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  onChange = (e) => {
    this.setState({
      placement: e.target.value,
    });
  }

  render() {
    const {menuItems, pathname, navTo} = this.props
    return (
      <div>
        <img src={menuIcon} style={{'paddingLeft': '20px', 'height':'40px', 'width':'40px'}} onClick={this.showDrawer} />
        <Icon type="drawer-handle" />
        <Drawer
          title="Richard Ryan Garcia"
          placement={this.state.placement}
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
        >
          <Menu
            theme="light"
            mode="vertical"
            defaultSelectedKeys={[`${pathname}`]}
            style={{ 'borderRight': '0px'}}
            align=''>

            {menuItems.map(item => (
              <Menu.Item
                key={item.path}
                onClick={()=>{navTo(`${item.path}`)}}>
                {item.label}
              </Menu.Item>
            ))}

          </Menu>
        </Drawer>
      </div>
    );
  }
}

export default MenuDrawer
