import React from 'react'
import { Layout, Menu, Breadcrumb } from 'antd';
import ResponsiveHeader from './header'

const { Header, Content, Footer } = Layout;


const PageLayout = ({children}) => {
  return (
    <Layout className="layout">
      <ResponsiveHeader />
      <Content style={{ padding: '30px 50px', background: '#fff' }}>
        <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>{children}</div>
      </Content>
      <Footer style={{ textAlign: 'center', background: '#fff' }}>
         Designed and Created by Richard Ryan Garcia ©2019
      </Footer>
    </Layout>
  )
}

export default PageLayout
