import React from 'react'
import {connect} from 'react-redux'
import { Layout, Menu, Breadcrumb, Icon, PageHeader, Row, Col } from 'antd';
import { push } from 'react-router-redux'
import logo from '../menu-icon.svg';
import Drawer from '../drawer'

const { Header, Content, Footer } = Layout;

const menuItems = [
  // System Pages
  {
    path: '/linkedin',
    label: 'Home',
  },
  // {
  //   path: '/activity',
  //   label: 'Activity',
  // },
  // {
  //   path: '/health',
  //   label: 'Health',
  // },
  // {
  //   path: '/music',
  //   label: 'Music',
  // },
  // {
  //   path: '/twitter',
  //   label: 'Twitter',
  // },
]

class DesktopHeader extends React.Component {

  navigateTo = (path) => {
    const {dispatch} = this.props
    dispatch(push(path))
  }

  render() {
    const {isMobile} = this.props.layout
    const {pathname} = this.props.location
    if (isMobile) {
      return (
        <Header style={{padding:'0px'}} >
            <Menu
              theme="light"
              mode="horizontal"
              style={{ lineHeight: '64px' }}>
                <Drawer menuItems={menuItems} pathname={pathname} navTo={this.navigateTo}/>
              </Menu>
        </Header>
      )
    } else {
      return (
        <Header style={{ position: 'fixed', zIndex: 1, width: '100%', padding: '0px' }}>
          <Menu
            theme="light"
            mode="horizontal"
            defaultSelectedKeys={[`${pathname}`]}
            style={{ lineHeight: '64px',  borderBottom:'0px', boxShadow: '0 4px 2px -2px lightgray' }}
            align=''>
            {/* <Menu.Item
              key='linkedin'
              onClick={()=>{this.navigateTo('/linkedin')}}>
              Richard Ryan Garcia
            </Menu.Item> */}

            {menuItems.map(item => (
              <Menu.Item
                key={item.path}
                onClick={()=>{this.navigateTo(`${item.path}`)}}>
                {item.label}
              </Menu.Item>
            ))}

          </Menu>
        </Header>
      )
    }
  }
}

const mapStateToProps = (state, ownProps) => ({ layout: state.layout, location: state.router.location })

export default connect(mapStateToProps, null)(DesktopHeader)
