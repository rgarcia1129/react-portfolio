import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import App from './App'
import ActivityPage from './pages/activity'
import HealthPage from './pages/health'
import MusicPage from './pages/music'
import TwitterPage from './pages/twitter'
import LinkedinPage from './pages/linkedin'

//
// import Loader from 'components/LayoutComponents/Loader'
// import NotFoundPage from 'pages/404'

const routes = [
  // System Pages
  {
    path: '/linkedin',
    component: LinkedinPage,
    exact: true,
  },
  // {
  //   path: '/home',
  //   component: App,
  //   exact: true,
  // },
  // {
  //   path: '/health',
  //   component: HealthPage,
  //   exact: true,
  // },
  // {
  //   path: '/music',
  //   component: MusicPage,
  //   exact: true,
  // },
  // {
  //   path: '/twitter',
  //   component: TwitterPage,
  //   exact: true,
  // },
]

class Router extends React.Component {
  render () {
    const {history} = this.props
    return (
      <ConnectedRouter history={history}>
        {/* <Layout> */}
          <Switch>
            <Route exact path="/" render={() => <Redirect to="/linkedin" />} />
            {routes.map(route => (
              <Route
                path={route.path}
                component={route.component}
                key={route.path}
                exact={route.exact}
              />
            ))}
            {/* <Route component={NotFoundPage} /> */}
          </Switch>
        {/* </Layout> */}
      </ConnectedRouter>
    )
  }
}

export default Router
