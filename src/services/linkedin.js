import axios from '../config/axios'

export const getAccountInfo = async () => {
  return axios.get('/linkedin/accountInfo')
}
