import actions from './actions'

const initialState = {
  isMobile: (typeof window.orientation !== 'undefined' || navigator.userAgent.indexOf('IEMobile') !== -1)
}

export default function layoutReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_STATE:
      return { ...state, ...action.payload }
    default:
      return state
  }
}
