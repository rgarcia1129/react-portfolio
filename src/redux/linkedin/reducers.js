import actions from './actions'

const initialState = {
  accountData: null,
}

export default function linkedinReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_STATE:
      return { ...state, ...action.payload }
    default:
      return state
  }
}
