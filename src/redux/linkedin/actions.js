const actions = {
  SET_STATE: 'linkedin/SET_STATE',
  GET_DATA: 'linkedin/GET_DATA',
}

export default actions
