import { all, put, call, takeEvery } from 'redux-saga/effects'
import { getAccountInfo } from '../../services/linkedin'
import actions from './actions'

export function* GET_DATA() {
  const accountInfo = yield call(getAccountInfo)
  const accountData = accountInfo.data.MOCKED_RESPONSE

  yield put({
    type: 'linkedin/SET_STATE',
    payload: {
      accountData
    },
  })
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.GET_DATA, GET_DATA)
  ])
}
