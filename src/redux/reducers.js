import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import layout from './layout/reducers'
import linkedin from './linkedin/reducers'

export default history =>
  combineReducers({
    router: connectRouter(history),
    layout,
    linkedin
  })
