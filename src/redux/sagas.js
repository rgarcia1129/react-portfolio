import { all } from 'redux-saga/effects'
import linkedin from './linkedin/sagas'


export default function* rootSaga() {
  yield all([linkedin()])
}
