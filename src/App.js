import React, { Component } from 'react';
import logo from './logo.svg';
import PageLayout from './components/pageLayout'
import './App.css';

class App extends Component {
  render() {
    return (
      <PageLayout>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
      </PageLayout>
    );
  }
}

export default App;
