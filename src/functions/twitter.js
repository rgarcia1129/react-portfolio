import twitter from 'twitter';


const client = new twitter({
  consumer_key: process.env.REACT_APP_CONSUMER_KEY,
  consumer_secret: process.env.REACT_APP_CONSUMER_SECRET,
  access_token_key: process.env.REACT_APP_ACCESS_TOKEN_KEY,
  access_token_secret: process.env.REACT_APP_ACCESS_TOKEN_SECRET
});

exports.handler = (event, context, callback) => {
  console.log('in lambda')
  console.log(client)
  var params = {screen_name: 'nodejs'};
  client.get('statuses/user_timeline', params, function(error, tweets, response) {
    if (!error) {
      console.log(tweets);
    }
  })
}
