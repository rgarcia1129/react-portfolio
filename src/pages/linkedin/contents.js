import React from 'react';
import {Row, Col, Card, Avatar, Collapse, Tag} from 'antd'
const Panel = Collapse.Panel;
const {Meta} = Card;

const LinkedInContent = (props) => {
  const {accountData} = props;
  return (<div>
      <Card bordered={false} style={{textAlign:'center'}}>
        <Avatar size={150} src={accountData.image_url } />
        <br />
        <br />
        <Card.Meta
        title={accountData ? accountData.headline : ''}
        description={accountData ? accountData.description : ''}
        />
      </Card>

      <Card title=" " bordered={false}>
        <Meta style={{paddingBottom: '20px'}} title='Education' />
          <p><span style={{ fontWeight:'700'}}>
            {accountData.education.degree}</span><br/>
            {accountData.education.school}<br/>
            {accountData.education.stop}
          </p>
          <p><Avatar size={64}  shape='square' src={accountData.education.image_url} /></p>
      </Card>

      <Card title=" " bordered={false} >
        <Meta style={{paddingBottom: '20px'}} title='Experience' />
        {accountData.jobs.map(job => (
          <div style={{paddingBottom: '30px'}}>
            <p><span style={{ fontWeight:'700'}}>
              {job.role} <br />
              </span>
              {job.title} <br />
            {job.start} - {job.stop}</p>
            <p>
              <Avatar shape='square' size={64} src={job.image_url} /> {'  '}
            </p>
            <p>{job.description}</p>
          </div>
        ))}
      </Card>

      <Card title=" " bordered={false} >
        <Meta style={{paddingBottom: '20px'}} title='Skills' />
        {accountData.skills.map(skill => (
          <Tag color="#108ee9">{skill}</Tag>
        ))}
      </Card>

      <Card title=" " bordered={false} >
        <Meta style={{paddingBottom: '20px'}} title='Projects' />
        <Tag><a href="https://github.com/rgarcia1129">Github</a></Tag>
        <Tag><a href="https://gitlab.com/rgarcia1129">Gitlab</a></Tag>
        <Tag><a href="https://devpost.com/software/8trac">8trac</a></Tag>
        <Tag><a href="https://gitlab.com/rgarcia1129/react-portfolio">Portfolio Client</a></Tag>
        <Tag><a href="https://github.com/rgarcia1129/portfolio-server">Portfolio Server</a></Tag>
      </Card>
    </div>
  )
}

export default LinkedInContent;
