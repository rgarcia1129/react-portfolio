import React from 'react'
import PageLayout from '../../components/pageLayout'
import { connect } from 'react-redux'
import {Card, Spin} from 'antd';
import LinkedInContent from './contents'

class Linkedin extends React.Component {
  componentDidMount(){
    const {dispatch} = this.props;
    dispatch({
      type: 'linkedin/GET_DATA',
    })
  }

  render () {
    const {accountData} = this.props
    return <div>
      <PageLayout>
        {accountData ? <LinkedInContent accountData={accountData} /> : (
          <Card bordered={false} style={{textAlign:'center', paddingTop:'100px'}}>
            <Spin tip='Loading...' size='large' />
          </Card>)}
      </PageLayout>
    </div>
  }
}

const mapStateToProps = state => {
  return {accountData: state.linkedin.accountData}
}

export default connect(mapStateToProps, null)(Linkedin)
