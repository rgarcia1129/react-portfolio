import axios from 'axios'

exports.handler = function(event, context, callback) {
    axios.post('http://requestbin.fullcontact.com/1c50pcg1', {name: 'richard'}).then((response)=>{
      callback(null, {
        statusCode: 200,
        body: response.body
      })
    }).catch((err) => {
      console.log(err)
    })
}
